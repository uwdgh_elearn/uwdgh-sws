<?php
/*
Plugin Name:  UW DGH | Student Web Service (Non-restricted Resources)
Plugin URI:   https://bitbucket.org/uwdgh_elearn/uwdgh-sws
Description:  A WordPress plugin that enables access to non-restricted resources of the UW Student Web Service (SWS) API. Requires an Access Token from UW-IT.
Author:       dghweb, Department of Global Health - University of Washington
Author URI:   https://depts.washington.edu/dghweb/
Version:      0.5.1
License:      GPLv2 or later
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  uwdgh-sws
Domain Path:
Bitbucket Plugin URI:  https://bitbucket.org/uwdgh_elearn/uwdgh-sws

"UW DGH | Student Web Service" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

"UW DGH | Student Web Service" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "UW DGH | Student Web Service". If not, see https://www.gnu.org/licenses/gpl-2.0.html.
 */
?>
<?php

if ( !class_exists( 'UWDGH_SWS' ) ) {

  include( plugin_dir_path( __FILE__ ) . 'inc/class-uw-student-web-service.php' );

  class UWDGH_SWS {

    static $SWS;
    static $markup;
    static $settingsupdated;

    /**
    * class initializaton
    */
    public static function init() {
      // IsPostback
      self::$settingsupdated = false;

      // add vendor script https://github.com/rstacruz/details-polyfill
      wp_register_script('uwdgh_sws_details_polyfill', plugin_dir_url(__FILE__) . 'js/vendor/details-polyfill-master/index.js', array(), false, true);
      wp_enqueue_script('uwdgh_sws_details_polyfill');

      // add plugin script
      wp_register_script('uwdgh_sws', plugin_dir_url(__FILE__) . 'js/uwdgh-sws.js', array('jquery'));
      wp_enqueue_script('uwdgh_sws');

      // add plugin stylesheet
      wp_enqueue_style( 'uwdgh_sws', plugin_dir_url(__FILE__) . 'css/uwdgh-sws.css' );

      // Ensure get_home_path() is declared.
      require_once( ABSPATH . 'wp-admin/includes/file.php' );

      // instantiate SWS
      self::$SWS = new UW_Student_Web_Service();

      // add menu item
      add_action( 'admin_menu' , array( __CLASS__, 'uwdgh_sws_settings' ) );
      // add function catching settings-updated
      add_action( 'admin_menu' , array( __CLASS__, 'uwdgh_sws_settings_updated' ) );

      // register plugin settings
      register_setting(
        'uwdgh_sws_options',
        'uwdgh_sws_options_auth_access_token',
        array(
          'type' => 'string',
          'description' => 'authentication token',
          'default' => '',
        )
      );
      register_setting(
        'uwdgh_sws_options',
        'uwdgh_sws_options_transient_expiration',
        array(
          'type' => 'number',
          'description' => 'transient expiration',
          'default' => 24,
        )
      );

      // register shortcode
      add_shortcode('uwdgh_sws_course_list', array( __CLASS__, 'uwdgh_sws_course_list_shortcode' ));

      // add metabox with shortcode help documentation
      add_action( 'add_meta_boxes_page',  array( __CLASS__, 'uwdgh_sws_add_meta_box' ));

      // register deactivation hook
      if( function_exists('register_deactivation_hook') )
        register_deactivation_hook(__FILE__,array( __CLASS__, 'uwdgh_sws_deactivate' ));

      // register uninstall hook
      if( function_exists('register_uninstall_hook') )
        register_uninstall_hook(__FILE__,array( __CLASS__, 'uwdgh_sws_uninstall' ));

    }

    /**
    * Admin menu item
    */
    function uwdgh_sws_settings() {
      add_options_page(
        'UW DGH | Student Web Service', //page title
        'UW DGH | SWS', //menu title
        'manage_options', //capability
        'uwdgh-sws',  //menu_slug
        array( __CLASS__, 'uwdgh_sws_options_page' )  //callback function
      );
    }

    /**
    * Is Postback from submitting the option form
    */
    function uwdgh_sws_settings_updated() {
      self::$settingsupdated = $_GET['settings-updated'];
    }

    /**
    * Options page
    */
    function uwdgh_sws_options_page() {

      // Verify "Test API Connection" link
      if ( $_GET['sws-test'] == 'true' ) {
        if ( wp_verify_nonce($_GET['_wpnonce'], 'uwdgh-sws-test') != false ) {
          self::$SWS->testRequest( get_option('uwdgh_sws_options_auth_access_token') );
        } else {
          self::$SWS::uwdgh_sws_warning_notice('There was an issue. The API connection test was not successful.');
        }
      }

      // Verify "CLear Cache" link
      if ( $_GET['sws-clear-cache'] == 'true' ) {
        if ( wp_verify_nonce($_GET['_wpnonce'], 'uwdgh-sws-clear-cache') != false ) {
          self::uwdgh_sws_clear_transient();
          self::$SWS::uwdgh_sws_info_notice('Cache cleared.');
        } else {
          self::$SWS::uwdgh_sws_warning_notice('There was an issue. Cache has not been cleared.');
        }
      }

      // Verify possible token update
      if ( self::$settingsupdated )
        self::$SWS->testRequest( get_option('uwdgh_sws_options_auth_access_token') );

      ?>
      <div class="wrap">
        <h2><?php _e('UW DGH | Student Web Service','uwdgh-sws');?></h2>
        <p><?php _e('This plugin allows the retrieval of resources from the UW Student Web Service. An Access Token for non-restricted resources is required.','uwdgh-sws');?></p>
        <p><?php _e('Refer to &quot;<a href="https://wiki.cac.washington.edu/display/SWS/Getting+Access+to+SWS" target="_blank">Getting Access to SWS</a>&quot;<span aria-hidden="true" class="dashicons dashicons-external"></span> to obtain an access token that can be used with this plugin.','uwdgh-sws');?></p>
        <p><?php _e('Here you can set or edit the fields needed for the plugin.','uwdgh-sws');?></p>
        <form action="options.php" method="post" id="uwdgh-sws-options-form">
          <?php settings_fields('uwdgh_sws_options'); ?>
          <table class="form-table">
            <tr class="even" valign="top">
              <th scope="row">
                <label for="uwdgh_sws_options_auth_access_token">
                  <?php _e('Access Token for non-restricted resources','uwdgh-sws');?>
                </label>
              </th>
              <td>
                <input type="text" size="64" minlength="36" maxlength="36" id="uwdgh_sws_options_auth_access_token" name="uwdgh_sws_options_auth_access_token" placeholder="Your UW SWS authentication token for non-restricted resources" value="<?php echo sanitize_text_field( get_option('uwdgh_sws_options_auth_access_token') ); ?>" />
              </td>
            </tr>
            <tr class="odd" valign="top">
                <th scope="row">
                    <label for="uwdgh_sws_options_transient_expiration">
                        <?php _e('Cache expiration','uwdgh-sws');?>
                    </label>
                </th>
                <td>
                  <select id="uwdgh_sws_options_transient_expiration" name="uwdgh_sws_options_transient_expiration">
                    <option value="0" <?php selected( get_option('uwdgh_sws_options_transient_expiration'), 0 ); ?>>No expiration (not recommended)</option>
                    <option value="12" <?php selected( get_option('uwdgh_sws_options_transient_expiration'), 12 ); ?>>12 hours</option>
                    <option value="24" <?php selected( get_option('uwdgh_sws_options_transient_expiration'), 24 ); ?>>1 day (24 hours)</option>
                    <option value="48" <?php selected( get_option('uwdgh_sws_options_transient_expiration'), 48 ); ?>>2 days</option>
                    <option value="72" <?php selected( get_option('uwdgh_sws_options_transient_expiration'), 72 ); ?>>3 days</option>
                    <option value="96" <?php selected( get_option('uwdgh_sws_options_transient_expiration'), 96 ); ?>>4 days</option>
                    <option value="120" <?php selected( get_option('uwdgh_sws_options_transient_expiration'), 120 ); ?>>5 days</option>
                    <option value="168" <?php selected( get_option('uwdgh_sws_options_transient_expiration'), 168 ); ?>>1 week</option>
                    <option value="336" <?php selected( get_option('uwdgh_sws_options_transient_expiration'), 336 ); ?>>2 weeks</option>
                    <option value="504" <?php selected( get_option('uwdgh_sws_options_transient_expiration'), 504 ); ?>>3 weeks</option>
                    <option value="672" <?php selected( get_option('uwdgh_sws_options_transient_expiration'), 672 ); ?>>4 weeks</option>
                  </select>
                  <p class="description"><?php _e('SWS query results are cached to optimize page loading. When the cache timeout expires, a page load will trigger a new query to the SWS and refresh the data.','uwdgh-sws');?></p>
                </td>
            </tr>
          </table>
          <?php submit_button(); ?>
        </form>
        <hr>
        <table class="form-table">
          <tr valign="top">
            <th>Verify token:</th>
            <td><a href="<?php echo self::uwdgh_sws_get_testlink(); ?>" class="button button-secondary"><?php _e('Test API Connection','uwdgh-sws');?></a></td>
          </tr>
          <tr valign="top">
            <th>Clear transient cache:</th>
            <td><a href="<?php echo self::uwdgh_sws_get_cclink(); ?>" class="button button-secondary"><?php _e('Clear Cache','uwdgh-sws');?></a>
              <p class="description">This will delete all cached transients for this plugin. Only use this if you're experiencing issues with the cached data.<p>
            </td>
          </tr>
        </table>
      </div>

      <?php
    }

    /**
    * Create the test link
    */
    function uwdgh_sws_get_testlink() {
      $testlink = get_admin_url( null, 'options-general.php', 'admin' );
      $testlink = add_query_arg( 'page', 'uwdgh-sws', $testlink);
      $testlink = add_query_arg( 'sws-test', 'true', $testlink);
      $testlink = add_query_arg( '_wpnonce', wp_create_nonce( 'uwdgh-sws-test' ), $testlink );
      return $testlink;
    }
    /**
    * Create the clear-cache link
    */
    function uwdgh_sws_get_cclink() {
      $cclink = get_admin_url( null, 'options-general.php', 'admin' );
      $cclink = add_query_arg( 'page', 'uwdgh-sws', $cclink);
      $cclink = add_query_arg( 'sws-clear-cache', 'true', $cclink);
      $cclink = add_query_arg( '_wpnonce', wp_create_nonce( 'uwdgh-sws-clear-cache' ), $cclink );
      return $cclink;
    }

    /**
    * Callback for shortcode uwdgh_sws_course_list
    */
    function uwdgh_sws_course_list_shortcode($atts = [], $content = null, $tag = '') {
      // map shortcode attributes
      $querystring_atts = [
        "curriculum_abbreviation" => str_replace("&amp;", "&", $atts["curriculum_abbreviation"]),
        "year"  => $atts["year"],
        "quarter" => $atts["quarter"],
        "exclude_courses_without_sections" => $atts["exclude_courses_without_sections"],
        //"transcriptable_course" => $atts["transcriptable_course"],
      ];
      // add page_size if empty
      ($atts['page_size']) ? $querystring_atts['page_size'] = $atts['page_size'] : $querystring_atts['page_size'] = 999 ;

      $querystring = self::format_query_string($querystring_atts);
      $url = self::$SWS::DOMAIN . self::$SWS::COURSE_ENDPOINT . "?" . $querystring;
      $response = self::$SWS->getCURL($url, get_option('uwdgh_sws_options_auth_access_token'), get_option('uwdgh_sws_options_transient_expiration'));
      if ($response instanceof Exception) {
        self::$markup = self::$SWS::uwdgh_sws_error_notice($response->getMessage());
      } else {
        $section_id = chr(rand(97,122)) . random_int(1000000, 2147483647) . chr(rand(65,90));
        self::$markup = '<section id="uwdgh-sws-course-list-' . $section_id . '" class="uwdgh-sws-course-list">' . PHP_EOL;
        self::$markup .= '<h5 class="uwdgh-sws-course-list-header uwdgh-sws-course-list-header--'.$response->Current->Quarter.'">' . PHP_EOL;
        self::$markup .= '<div style="display: inline-block;">' . $response->Current->CurriculumAbbreviation . ', ' . $response->Current->Quarter . ' ' . $response->Current->Year . '</div>' . PHP_EOL;
        self::$markup .= '<div class="uwdgh-sws-course-list-toggle-wrapper"><a href="#" data-uwdgh-sws-course-list-toggle>open all</a></div>' . PHP_EOL;
        self::$markup .= '</h5>' . PHP_EOL;
        if ( $atts['display_filter']=="on" )
          self::uwdgh_sws_markup_course_list_filter($section_id) ;
        if ($response->TotalCount > 0) {
          // markup response
          self::uwdgh_sws_markup_course_list($response);
        } else {
          self::$markup .= '<div class="uwdgh-sws-course-list-content">There are presently no courses in the requested curriculum "' . $response->Current->CurriculumAbbreviation . '" scheduled for quarter "' . $response->Current->Quarter . ' ' . $response->Current->Year . '". Please check back later.</div>' . PHP_EOL;
        }
        self::$markup .= '<footer><em><small>';
        self::$markup .= 'Please refer to the <a target="_blank" href="https://www.washington.edu/students/timeschd/" title="University of Washington Time Schedule">UW Time Schedule</a> <span aria-hidden="true" class="dashicons dashicons-external"></span> for course availability and other course specifics, or contact the department for verification.';
        self::$markup .= '</small></em></footer>' . PHP_EOL;
        self::$markup .= '</section>' . PHP_EOL;
      }
      return self::$markup;
    }

    /**
    * Markup response
    */
    function uwdgh_sws_markup_course_list($response) {
      foreach ($response->Courses as $Course) {
        // get course details
        $url = self::$SWS::DOMAIN . $Course->Href ;
        $CourseDetails = self::$SWS->getCURL($url, get_option('uwdgh_sws_options_auth_access_token'), get_option('uwdgh_sws_options_transient_expiration'));
        if ($CourseDetails instanceof Exception) {
          self::$markup = self::$SWS::uwdgh_sws_error_notice($CourseDetails->getMessage());
        } else {
          switch ($CourseDetails->CreditControl) {
            case 'fixed credit':
              $_credits = "(" . $CourseDetails->MinimumTermCredit . ")";
              break;
            case 'variable credit - min to max credits':
              $_credits = "(" . $CourseDetails->MinimumTermCredit . "-" . $CourseDetails->MaximumTermCredit . ")";
              break;
            default:
              $_credits = "";
              break;
          }
          self::$markup .= '<details class="uwdgh-sws-course-list-course" data-uwdgh-sws-course-number="' . $Course->CourseNumber . '">' . PHP_EOL;
          self::$markup .= '<summary><span title="curriculum">' . $Course->CurriculumAbbreviation . '</span> <span title="course number">' . $Course->CourseNumber . '</span> - <span title="course title">' . $Course->CourseTitle . '</span> <span title="credits">' . $_credits . '</span></summary>' . PHP_EOL;
          self::$markup .= '<div class="uwdgh-sws-course-list-content">' . PHP_EOL;
          self::$markup .= '<h6>' . $Course->CurriculumAbbreviation . ' ' . $Course->CourseNumber . ' - ' . $Course->CourseTitleLong . '</h6>' . PHP_EOL;
          self::$markup .= '<p>' . $CourseDetails->CourseDescription . '</p>' . PHP_EOL;
          if ($CourseDetails->CourseCampus) { self::$markup .= '<p>' . $CourseDetails->CourseCampus . ' Campus</p>' . PHP_EOL; }
          // get course sections
          $querystring_atts = [
            "curriculum_abbreviation" => $response->Current->CurriculumAbbreviation,
            "year"  => $response->Current->Year,
            "quarter" => $response->Current->Quarter,
            "course_number" => $Course->CourseNumber,
          ];
          $querystring = self::format_query_string($querystring_atts);
          $url = self::$SWS::DOMAIN . self::$SWS::SECTION_ENDPOINT . "?" . $querystring;
          $CourseSections = self::$SWS->getCURL($url, get_option('uwdgh_sws_options_auth_access_token'), get_option('uwdgh_sws_options_transient_expiration'));
          if ($CourseSections instanceof Exception) {
            self::$markup = self::$SWS::uwdgh_sws_error_notice($CourseSections->getMessage());
          } else {
            if ($CourseSections->TotalCount > 0) {
              self::uwdgh_sws_markup_course_sections($CourseSections);
            } else {
              self::$markup .= '<p><mark>There are no course sections listed. This course may not be currently scheduled. Contact the department for verification.</mark></p>' . PHP_EOL;
            }
          }
          // Course link to MyPlan
          self::$markup .= '<div class="uwdgh-sws-course-list-link">' . PHP_EOL;
          self::$markup .= '<a href="https://myplan.uw.edu/course/#/courses/' . $Course->CurriculumAbbreviation . $Course->CourseNumber;
          self::$markup .= '" title="MyPlan" target="_blank">View ' . $Course->CurriculumAbbreviation . ' ' . $Course->CourseNumber . ' in MyPlan</a> <span aria-hidden="true" class="dashicons dashicons-external"></span>' . PHP_EOL;
          self::$markup .= '</div>' . PHP_EOL;
          self::$markup .= '</div>' . PHP_EOL;
          self::$markup .= '</details>' . PHP_EOL;
        }
      }
    }

    /**
    * Markup Course Sections
    */
    function uwdgh_sws_markup_course_sections($CourseSections) {
      $TimeScheduleLink = '';
      self::$markup .= '<details>';
      self::$markup .= '<summary title="Course Sections">Course Sections</summary>' . PHP_EOL;
      self::$markup .= '<div style="overflow-x: auto;">' . PHP_EOL;
      self::$markup .= '<table>' . PHP_EOL;
      self::$markup .= '<thead>' . PHP_EOL;
      self::$markup .= '<tr>' . PHP_EOL;
      self::$markup .= '<th>Section</th>' . PHP_EOL;
      self::$markup .= '<th>SLN</th>' . PHP_EOL;
      self::$markup .= '<th>Credits</th>' . PHP_EOL;
      self::$markup .= '<th>Instructor(s)</th>' . PHP_EOL;
      self::$markup .= '<th>Comments</th>' . PHP_EOL;
      self::$markup .= '</tr>' . PHP_EOL;
      self::$markup .= '</thead>' . PHP_EOL;
      self::$markup .= '<tbody>' . PHP_EOL;
      foreach ($CourseSections->Sections as $Section) {
        $url = self::$SWS::DOMAIN . $Section->Href ;
				//do_action('qm/debug', $url);
        $SectionDetails = self::$SWS->getCURL($url, get_option('uwdgh_sws_options_auth_access_token'), get_option('uwdgh_sws_options_transient_expiration'));
        if ($SectionDetails instanceof Exception) {
          self::$markup = self::$SWS::uwdgh_sws_error_notice($SectionDetails->getMessage());
        } else {
          // skip the following
          if ( ($SectionDetails->InstituteName == "UW IN THE HIGH SCHOOL") || ($SectionDetails->InstituteName == "WWAMI")) {
            continue;
          }
          self::$markup .= '<tr>' . PHP_EOL;
          self::$markup .= '<td>' . $SectionDetails->SectionID . '</td>' . PHP_EOL;
          self::$markup .= '<td>' . $SectionDetails->SLN . '</td>' . PHP_EOL;
            switch ($SectionDetails->CreditControl) {
              case 'fixed credit':
                self::$markup .= '<td>' . round($SectionDetails->MinimumTermCredit) . '</td>' . PHP_EOL;
                break;
              case 'variable credit - min to max credits':
                self::$markup .= '<td>' . round($SectionDetails->MinimumTermCredit) . "-" . round($SectionDetails->MaximumTermCredit) . '</td>' . PHP_EOL;
                break;
              default:
                self::$markup .= '<td></td>' . PHP_EOL;
                break;
            }
          self::$markup .= '<td>';
          foreach ($SectionDetails->Meetings[0]->Instructors as $Instructor) {
            if ($Instructor->TSPrint)
              self::$markup .= $Instructor->Person->Name . '<br>';
          }
          self::$markup .= '</td>' . PHP_EOL;
          self::$markup .= '<td>';
          self::$markup .= '<p>';
          foreach ($SectionDetails->TimeScheduleComments->SectionComments->Lines as $SectionComment) {
            self::$markup .= $SectionComment->Text . '<br>';
          }
          foreach ($SectionDetails->TimeScheduleComments->TimeScheduleGeneratedComments->Lines as $TimeScheduleGeneratedComments) {
            self::$markup .= $TimeScheduleGeneratedComments->Text . '<br>';
          }
          self::$markup .= '</p>' . PHP_EOL;
          self::$markup .= '</td>' . PHP_EOL;
          self::$markup .= '</tr>' . PHP_EOL;
          if( ($SectionDetails->CourseCampus)
            && ($SectionDetails->Course->Year)
            && ($SectionDetails->Course->Quarter)
            && ($SectionDetails->Curriculum->TimeScheduleLinkAbbreviation)
            && ($SectionDetails->Course->CurriculumAbbreviation)
            && ($SectionDetails->Course->CourseNumber) ) {
            $timeschd_args = [
              "campus" => $SectionDetails->CourseCampus,
              "year" => $SectionDetails->Course->Year,
              "quarter" => $SectionDetails->Course->Quarter,
              "curriculum_abbreviation" => $SectionDetails->Curriculum->TimeScheduleLinkAbbreviation,
            ];
            $TimeScheduleLink = self::uwdgh_timeschd_link($timeschd_args);
            $TimeScheduleLink .= '#' . strtolower(str_replace(' ', '', $SectionDetails->Course->CurriculumAbbreviation)) . $SectionDetails->Course->CourseNumber;
          }
        }
      }
      self::$markup .= '</tbody>' . PHP_EOL;
      self::$markup .= '<tfoot>';
      self::$markup .= '<tr>' . PHP_EOL;
      self::$markup .= '<td colspan="5"><small><em>Please refer to the ';
      if (!$TimeScheduleLink)
        $TimeScheduleLink = 'https://www.washington.edu/students/timeschd/';
      self::$markup .= '<a href="' . $TimeScheduleLink . '" title="University of Washington Time Schedule" target="_blank">' . ucfirst($CourseSections->Current->Quarter) . ' Quarter '. ucfirst($CourseSections->Current->Year) . ' Time Schedule</a> <span aria-hidden="true" class="dashicons dashicons-external"></span>';
      self::$markup .= ' for course availability and other course specifics.</em></small></td>' . PHP_EOL;
      self::$markup .= '</tr>' . PHP_EOL;
      self::$markup .= '</tfoot>' . PHP_EOL;
      self::$markup .= '</table>' . PHP_EOL;
      self::$markup .= '</div>' . PHP_EOL;
      self::$markup .= '</details>' . PHP_EOL;
    }

      /**
      * Create Time Schedule link
      */
      function uwdgh_timeschd_link ($atts) {
        switch ($atts["campus"]) {
          case 'Bothell':
            $_campus = "B/";
            break;
          case 'Tacoma':
            $_campus = "T/";
            break;
          default:
            $_campus = "";
            break;
        }
        switch ($atts["quarter"]) {
          case 'winter':
            $_quarter = "WIN";
            break;
          case 'spring':
            $_quarter = "SPR";
            break;
          case 'summer':
            $_quarter = "SUM";
            break;
          case 'autumn':
            $_quarter = "AUT";
            break;
        }

        return "https://www.washington.edu/students/timeschd/".$_campus.$_quarter.$atts["year"]."/".$atts["curriculum_abbreviation"].".html";
      }

    /**
    * Add filter to Markup response
    */
    function uwdgh_sws_markup_course_list_filter($section_id) {
      self::$markup .= '<form class="uwdgh-sws-course-list-filter">' . PHP_EOL;
      self::$markup .= '<label class="uwdgh-sws-course-list-filter__label" for="uwdgh-sws-filter-' . $section_id . '-a"><input id="uwdgh-sws-filter-' . $section_id . '-a" name="uwdgh-sws-filter-' . $section_id . '" type="radio" value="a" checked>all courses</label>';
      self::$markup .= '<label class="uwdgh-sws-course-list-filter__label" for="uwdgh-sws-filter-' . $section_id . '-u"><input id="uwdgh-sws-filter-' . $section_id . '-u" name="uwdgh-sws-filter-' . $section_id . '" type="radio" value="u">undergraduate courses</label>';
      self::$markup .= '<label class="uwdgh-sws-course-list-filter__label" for="uwdgh-sws-filter-' . $section_id . '-g"><input id="uwdgh-sws-filter-' . $section_id . '-g" name="uwdgh-sws-filter-' . $section_id . '" type="radio" value="g">graduate courses</label>';
      self::$markup .= '</form>' . PHP_EOL;
    }

    /**
     * Add custom metaboxes
     * @param $post
     */
    function uwdgh_sws_add_meta_box() {
      add_meta_box(
        'uwdgh-sws-shortcode-help-metabox', // Metabox HTML ID attribute
        'UW DGH | Student Web Service shortcode help', // Metabox title
        array( __CLASS__, 'uwdgh_sws_shortcode_help_metabox' ), // callback name
        null, // post type
        'normal', // context (advanced, normal, or side)
        'core' // priority (high, core, default or low)
      );
    }

    /**
    * Metabox content
    */
    function uwdgh_sws_shortcode_help_metabox () {
      ?>
      <h4>[uwdgh_sws_course_list]</h4>
      <p><em>example</em>:<br>[uwdgh_sws_course_list curriculum_abbreviation="G H" year="2020" quarter="spring" exclude_courses_without_sections="on" display_filter="on"]</p>
      <div><strong>Attributes:</strong></div>
      <div>
        <p>
          <code>curriculum_abbreviation</code><span style="color:red;margin-left:1em,font-family:monospace">&#42;&#32;required</span><br>
          The curriculum abbreviation attribute identifies the department through which the course is offered; for example, "ENGL" is English and "CSE" is Computer Science and Engineering. Sometimes curriculum abbreviation is referred to as department code.
          To be valid, a curriculum abbreviation must be uppercase, and between 2 and 6 alphabetic characters. Only two special characters are permitted, the ampersand ("&amp;") and a space; for example, "EDC&I" is Curriculum and Instruction, "G H" is Global Health.<br>
          A complete overview of available course descriptions (and their abbreviation) can be found at University of Washington Course Descriptions (<a target="_blank" href="https://www.washington.edu/students/crscat/" title="University of Washington Course Descriptions Seattle">www.washington.edu/students/crscat/</a> <span aria-hidden="true" class="dashicons dashicons-external"></span>)
        </p>
        <p>
          <code>year</code><span style="color:red;margin-left:1em,font-family:monospace">&#42;&#32;required</span><br>
          The curriculum year in 4 digits, e.g.: <var>2020</var>
        </p>
        <p>
          <code>quarter</code><span style="color:red;margin-left:1em,font-family:monospace">&#42;&#32;required</span><br>
          The curriculum quarter. Use one of these values: <var>winter</var>, <var>spring</var>. <var>summer</var>, <var>autumn</var>
        </p>
        <p>
          <code>exclude_courses_without_sections</code> (optional)<br>
          Leave this attribute as-is to only include courses with active sections.<br>
          Omit the entire attribute to include courses without sections. This will likely yield a larger result set.
        </p>
        <p>
          <code>display_filter</code> (optional)<br>
          The "on" value for this attribute will display an undergraduate/graduate filter for the user.<br>
          Omit the entire attribute if the filter is not needed.
        </p>
      </div>
      <?php
    }


    /**
     * Helper function
     * @param type $args
     * @return string
     */
    private function format_query_string($args) {
      // create the querystring
      $terms = count($args);
      $attributes = '';
      foreach ($args as $key => $value) {
        $terms--;
        $attributes .= $key . '=' . urlencode($value);
        ($terms) ? $attributes .= '&' : '' ;
      }
      return $attributes;
    }
    /**
    * helper funcion
    */
    private function print_r($response) {
      echo '<pre style="overflow:auto;max-height:40vh;">';
      print_r($response);
      echo '</pre>';
    }

    /**
    * Dispose plugin option upon plugin deletion
    */
    function uwdgh_sws_deactivate() {
      // remove shortcode registration
      remove_shortcode('uwdgh_sws_course_list');
      // clear transient cache
      self::uwdgh_sws_clear_transient();
    }

    /**
    * Dispose plugin option upon plugin deletion
    */
    function uwdgh_sws_uninstall() {
      // delete any options
      delete_option('uwdgh_sws_options_auth_access_token');
    }

    /**
    * Dispose transient cache for this plugin
    */
    function uwdgh_sws_clear_transient() {
      global $wpdb;
      $wpdb->query(
          $wpdb->prepare(
              "DELETE FROM $wpdb->options WHERE option_name LIKE %s",
              '%_transient%uwdgh_sws_%'
          )
      );
    }

  }

  UWDGH_SWS::init();

}

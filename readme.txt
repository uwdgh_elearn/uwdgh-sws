=== UW DGH | Student Web Service (Non-restricted Resources) ===
Contributors: jbleys
Requires at least: 4.4
Tested up to: 6.2
Requires PHP: 5.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==
A WordPress plugin that enables access to non-restricted resources of the UW Student Web Service (SWS). Requires an Access Authentication Token from UW-IT.

= How to enable this plugin =

[Request an Access Authentication token](https://wiki.cac.washington.edu/display/SWS/Getting+Access+to+SWS) for access to non-restricted resources.

After obtaining your Access Authentication token, activate the plugin and store your Access Token on the plugin's Settings page.

= How to use this plugin =

Use shortcode <strong>[uwdgh_sws_course_list]</strong> anywhere in your content.

_example_:<br>
[uwdgh_sws_course_list curriculum_abbreviation="G H" year="2020" quarter="autumn" exclude_courses_without_sections="on" display_filter="on"]

This will return a list of courses in an accordion presentation.<br>
[Example image](https://bitbucket.org/uwdgh_elearn/uwdgh-sws/src/master/assets/images/example.jpg)

__Attributes__:<br>
`curriculum_abbreviation` (required)<br>
The curriculum abbreviation attribute identifies the department through which the course is offered; for example, "ENGL" is English and "CSE" is Computer Science and Engineering. Sometimes curriculum abbreviation is referred to as department code. To be valid, a curriculum abbreviation must be uppercase, and between 2 and 6 alphabetic characters. Only two special characters are permitted, the ampersand ("&") and a space; for example, "EDC&I" is Curriculum and Instruction, "G H" is Global Health.
A complete overview of available course descriptions (and their abbreviation) can be found at University of Washington [Course Descriptions](https://www.washington.edu/students/crscat/)

`year` (required)<br>
The curriculum year in 4 digits, e.g.: 2020

`quarter` (required)<br>
The curriculum quarter. Use one of these values: _winter_, _spring_. _summer_, _autumn_

`exclude_courses_without_sections` (optional)<br>
Leave this attribute as-is to only include courses with active sections.
Omit the entire attribute to include courses without sections. This will likely yield a larger result set.

`display_filter` (optional)<br>
The "on" value for this attribute will display an undergraduate/graduate filter for the user.
Omit the entire attribute if the filter is not needed.


== Changelog ==
**[unreleased]**

#### 0.5.1/ 2023‑05-11
* Add TimeScheduleGeneratedComments for course sections.
* Hash the transient key.
* Use WP dashicon for external links.
* Remove uw-2014 theme styling.

#### 0.5/ 2023‑05-11
* Tested for 6.2.
* Updated Text Domain and removed Stable tag from readme header.

#### 0.4/ 2021‑10-14
* Updated external links.
* Consolidated changelog file.

#### 0.3.4/ 2020‑09-30
* Added option for cache expiration.
* Added "Clear Cache" link on admin page.
* Added nonce verifications for links on admin page.

#### 0.3.3/ 2020‑08-31
* Section table style updates.
* Hide last two section columns on smaller screens

#### 0.3.2/ 2020‑08-29
* Added section SLN and credits data.
* Updated the Time Schedule link.
* Allow scrollable overflow for section tables.

#### 0.3.1/ 2020‑08-26
* The course sections table is now nested inside a details element.
* Updated the Time Schedule link text to "`Quarter` Quarter `Year` Time Schedule".

#### 0.3/ 2020‑08-25
* Add quarter specific Time Schedule links to each section table.
* Skip sections for "UW IN THE HIGH SCHOOL" and "WWAMI".

#### 0.2.11 / 2020‑08-25
* Bug fix. URL encoding on all querystring values, and ampersand html code replacement.

#### 0.2.10 / 2020‑08-20
* List all sections instead of just the primary section. Occasionally the A section is not actually in use.

#### 0.2.9 / 2020‑08-14
* Added course primary section

#### 0.2.8 / 2020‑08-13
* Added plugin icon

#### 0.2.7 / 2020‑08-13
* Added instructions to plugin description

#### 0.2.6 / 2020‑08-13
* Bug fix. Make details-polyfill script load in footer
* Updated instruction text
* Added changelog and plugin banner image

#### 0.2.5 / 2020‑04‑01
* Updated instruction text
* CSS enhancements

#### 0.2.4 / 2020‑03‑30
* Plugin will use UW boundless theme styling when site's theme uses the UW boundless theme

#### 0.2.3 / 2020‑03‑21
* Updated instruction text
* Added open-close toggler for entire course list

#### 0.2.2 / 2020‑03‑19
* CSS enhancements
* Bug fixes

#### 0.2.1 / 2020‑03‑16
* Updated to MyPlan url
* Bug fixes

#### 0.2 / 2020‑03‑14
* Improved transient cache handling by deleting the transient data on deactivation. improved error handling in response markup.

#### 0.1 / 2020‑03‑12
* Initial commit

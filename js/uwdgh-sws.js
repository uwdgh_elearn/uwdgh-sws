(function ($, window, document) {

  // The document ready event executes when the HTML-Document is loaded
  // and the DOM is ready.
  jQuery(document).ready(function( $ ) {

        // open/close all toggle
        $(document).on("click", "a[data-uwdgh-sws-course-list-toggle]", function(e){
          e.preventDefault();
          link = e.target;
          courselist = $(this).closest('.uwdgh-sws-course-list');
          switch (link.text) {
            case 'open all':
              $( '#' + $(courselist).attr('id').toString() ).children('details').attr('open', 'open');
              link.text = 'close all';
              $(link).addClass('opened');
              break;
            case 'close all':
              $( '#' + $(courselist).attr('id').toString() ).children('details').removeAttr('open');
              link.text = 'open all';
              $(link).removeClass('opened');
              break;
            default:
              $( '#' + $(courselist).attr('id').toString() ).children('details').removeAttr('open');
              link.text = 'open all';
              $(link).removeClass('opened');
          }
        });

        // set to 'close all' if two or more details are open
        $(document).on("click", ".uwdgh-sws-course-list details", function(e){
          courselist = $(this).closest('.uwdgh-sws-course-list');
          courselist_id = '#' + $(courselist).attr('id').toString();
          // count all current open details
          count = $(courselist_id).children('details[open]').length;
          if ($(this).prop('open')) {
            // currently open and closing this details
            count = count - 1;
          } else {
            // currently closed and opening this details
            count = count + 1;
          }
          if (count > 1) {
            $( courselist_id + ' a[data-uwdgh-sws-course-list-toggle]')
              .text('close all')
              .addClass('opened');
          } else if (count == 0) {
            $( courselist_id + ' a[data-uwdgh-sws-course-list-toggle]')
              .text('open all')
              .removeClass('opened');
          }
        });

        // initialize text for each Course Sections summary element
        $(".uwdgh-sws-course-list-content summary").each(function () {
          $(this).text("Show course sections");
					// add bootstrap class
					$(this).addClass("btn-link");
        });
        // Show/Close cousre sections toggle
        $(document).on("click", ".uwdgh-sws-course-list-content summary", function(e){
          if ( $(this).parent("details").attr("open") ) {
            $(this).text("Show course sections");
          } else {
            $(this).text("Close course sections");
          }
        });

        // undergrad/grad filter
        $(document).on("change", ".uwdgh-sws-course-list input[type=radio]", function(e){
          input = e.target;
          courselist = $(input).closest('.uwdgh-sws-course-list');
          $( '#' + $(courselist).attr('id').toString() ).children('details').each(function () {
            switch (input.value) {
              case 'u':
                if ($(this).data("uwdgh-sws-course-number") < 500) {
                  $(this)
                    .css("display", "block")
                    .removeAttr("hidden");
                } else {
                  $(this)
                    .css("display", "none")
                    .prop("hidden", true);
                }
                break;
              case 'g':
                if ($(this).data("uwdgh-sws-course-number") < 500) {
                  $(this)
                    .css("display", "none")
                    .prop("hidden", true);
                } else {
                  $(this)
                    .css("display", "block")
                    .removeAttr("hidden");
                }
                break;
              default:
                $(this)
                  .css("display", "block")
                  .removeAttr("hidden");
            }
          });
        });

  });//document.ready

  // The window load event executes after the document ready event,
  // when the complete page is fully loaded.
  // jQuery(window).load(function () {
  //
  // });//window.load

})(jQuery, this, this.document);

<?php

/**
 * Description of UWStudentWebService
 *
 * @author jasper
 */
class UW_Student_Web_Service {

  const DOMAIN = "https://ws.admin.washington.edu";
  const CAMPUS = "campus";
  const CAMPUS_ENDPOINT = "/student/v5/campus.json";
  const COLLEGE = "college";
  const COLLEGE_ENDPOINT = "/student/v5/college.json";
  const COURSE = "course";
  const COURSE_ENDPOINT = "/student/v5/course.json";
  const CURRICULUM = "curriculum";
  const CURRICULUM_ENDPOINT = "/student/v5/curriculum.json";
  const DEPARTMENT = "department";
  const DEPARTMENT_ENDPOINT = "/student/v5/department.json";
  const RESOURCES = "resources";
  const RESOURCES_ENDPOINT = "/student/v5/resources.json";
  const SECTION = "section";
  const SECTION_ENDPOINT = "/student/v5/section.json";
  private $http_codes = '';

  /**
   * Constructor.
   */
  public function __construct() {
    // load the HTTP codes from the text file
    $this->http_codes = parse_ini_file( plugin_dir_path( __FILE__ ) . 'http_status.txt');
  }

  /**
   * getCURL
   * Initiates a cURL request or retrieves cached data based on url
   *
   * @staticvar type $my_data
   * @param type $url
   * @return type
   * @throws Exception
   */
  function getCURL($url, $token, $expiration_hours) {
		// transient key cannot be longer than 172 characters
		// so we must hash the url part to trim to a fixed length
    $_transient_key = 'uwdgh_sws_' . hash('sha1',$url);
    try {
      $_data = get_transient($_transient_key);
      if ($_data) {
        // get data from transient cache
        $response = unserialize($_data);
      } else {
        // Do expensive calculations here, and populate the transient cache
        $curl = curl_init();
        $httpheader = array(
            'Authorization: Bearer ' . $token,
        );
        curl_setopt_array($curl, array(
          CURLOPT_HEADER => FALSE,    // we don't want the header data back in the response
          CURLOPT_HTTPHEADER => $httpheader,
          CURLOPT_RETURNTRANSFER => TRUE,
          CURLOPT_URL => sanitize_url($url),
        ));
        $response = curl_exec($curl);
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $info = curl_getinfo($curl);
        curl_close($curl);
        if ($http_status === 200) {
          $response = json_decode($response);
          // populate transient cache, and set cache expiration in seconds
          set_transient($_transient_key, serialize($response), 3600 * $expiration_hours);
        } else {
          $response = new Exception($http_status . ': ' . $this->http_codes[$info['http_code']], $http_status);
        }
      }
      return $response;

    } catch (Exception $exc) {
      self::uwdgh_sws_error_notice( $exc->getMessage() );
    }
  }


  /**
   * test call to resources endpoint
   */
  public function testRequest($token) {
    try {
      $url = self::DOMAIN . self::CAMPUS_ENDPOINT;
      $curl = curl_init();
      $httpheader = array(
          'Authorization: Bearer ' . $token,
      );
      curl_setopt_array($curl, array(
        CURLOPT_HEADER => FALSE,
        CURLOPT_HTTPHEADER => $httpheader,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_URL => sanitize_url($url),
      ));
      $response = curl_exec($curl);
      $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      $info = curl_getinfo($curl);
      curl_close($curl);
      if ($http_status === 200) {
        self::uwdgh_sws_success_notice($http_status . ': ' . $this->http_codes[$info['http_code']]);
      } else {
        self::uwdgh_sws_error_notice($http_status . ': ' . $this->http_codes[$info['http_code']]);
        UWDGH_SWS::uwdgh_sws_clear_transient();
      }
    } catch (Exception $exc) {
      self::uwdgh_sws_error_notice( $exc->getMessage() );
    }
  }

    /**
     *
     * @param type $notice
     */
    public static function uwdgh_sws_info_notice($notice) {
        ?>
        <div class="notice notice-info is-dismissible">
            <p><?php _e( $notice ); ?></p>
        </div>
        <?php
    }
    /**
     *
     * @param type $notice
     */
    public static function uwdgh_sws_success_notice($notice) {
        ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e( $notice ); ?></p>
        </div>
        <?php
    }
    /**
     *
     * @param type $notice
     */
    public static function uwdgh_sws_warning_notice($notice) {
        ?>
        <div class="notice notice-warning is-dismissible">
            <p><?php _e( $notice ); ?></p>
        </div>
        <?php
    }
    /**
     *
     * @param type $notice
     */
    public static function uwdgh_sws_error_notice($notice) {
        ?>
        <div class="notice notice-error is-dismissible">
            <p><?php _e( $notice ); ?></p>
        </div>
        <?php
    }

}

# README #

This README document contains steps necessary to use the _UW DGH | Student Web Service_ WordPress plugin

## What is this repository for? ##

* A WordPress plugin that enables access to non-restricted resources of the UW Student Web Service (SWS). Requires an Access Authentication Token from UW-IT.

## How do I get set up? ##

* Navigate to your wp-content/plugins/ folder
* `git clone https://bitbucket.org/uwdgh_elearn/uwdgh-sws`
Alternatively you can use the [Github Updater](https://github.com/afragen/github-updater) plugin to manage themes and plugins not hosted on WordPress.org.

### How do I enable this plugin

[Request an Access Authentication token](https://wiki.cac.washington.edu/display/SWS/Getting+Access+to+SWS) for access to non-restricted resources.

After obtaining your Access Authentication token, activate the plugin and store your Access Token on the plugin's Settings page.

### How do I use this plugin

Use shortcode `[uwdgh_sws_course_list]` anywhere in your content.

_example_:  
`[uwdgh_sws_course_list curriculum_abbreviation="G H" year="2020" quarter="autumn" exclude_courses_without_sections="on" display_filter="on"]`

This will return a list of courses in an accordion presentation.  
[Example image](https://bitbucket.org/uwdgh_elearn/uwdgh-sws/src/master/assets/images/example.jpg)

#### Attributes:  
`curriculum_abbreviation` (required)  
The curriculum abbreviation attribute identifies the department through which the course is offered; for example, "ENGL" is English and "CSE" is Computer Science and Engineering. Sometimes curriculum abbreviation is referred to as department code. To be valid, a curriculum abbreviation must be uppercase, and between 2 and 6 alphabetic characters. Only two special characters are permitted, the ampersand ("&") and a space; for example, "EDC&I" is Curriculum and Instruction, "G H" is Global Health.
A complete overview of available course descriptions (and their abbreviation) can be found at University of Washington [Course Descriptions](https://www.washington.edu/students/crscat/)

`year` (required)  
The curriculum year in 4 digits, e.g.: 2020

`quarter` (required)  
The curriculum quarter. Use one of these values: _winter_, _spring_. _summer_, _autumn_

`exclude_courses_without_sections` (optional)  
Leave this attribute as-is to only include courses with active sections.
Omit the entire attribute to include courses without sections. This will likely yield a larger result set.

`display_filter` (optional)  
The "on" value for this attribute will display an undergraduate/graduate filter for the user.
Omit the entire attribute if the filter is not needed.


## Who do I talk to? ##

* dghweb@uw.edu
